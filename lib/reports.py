from __future__ import absolute_import, print_function, unicode_literals
from snack import *
import _snack
import sys
import os
import os.path
from . import utils
from .db import db
from .config import Config

def report(screen):
	labelDomains = Label("Domains:")
	labelMailboxes = Label("Mailboxes:")
	labelAliases = Label("Aliases:")
	bb = ButtonBar(screen, (("Back", "back"),))
	domains = db.selectone("SELECT COUNT(*) FROM virtual_domains;")
	emails = db.selectone("SELECT COUNT(*) FROM virtual_users;")
	aliases = db.selectone("SELECT COUNT(*) FROM virtual_aliases;")
	labelDomainsV = Label(str(domains[0]))
	labelDomainsV.setColors(_snack.COLORSET_TITLE)
	labelMailboxesV = Label(str(emails[0]))
	labelMailboxesV.setColors(_snack.COLORSET_TITLE)
	labelAliasesV = Label(str(aliases[0]))
	labelAliasesV.setColors(_snack.COLORSET_TITLE)
	g = Grid(2, 3)
	g.setField(labelDomains, 0, 0, anchorLeft = 1)
	g.setField(labelDomainsV, 1, 0, padding = (5, 0, 0, 0), anchorRight = 1)
	g.setField(labelMailboxes, 0, 1, anchorLeft = 1)
	g.setField(labelMailboxesV, 1, 1, anchorRight = 1)
	g.setField(labelAliases, 0, 2, anchorLeft = 1)
	g.setField(labelAliasesV, 1, 2, anchorRight = 1)
	gf = GridForm(screen, "REPORT", 1, 2)
	gf.add(g, 0, 0, padding = (0, 0, 0, 1))
	gf.add(bb, 0, 1)
	screen.refresh()
	result = gf.runOnce()

def reportQuota(screen):
	search = ""
	filter = ""
	buttons0 = ('Search', 'Back')
	while True:
		sok = ""
		snok = "!!! "
		items = db.select("SELECT email, quota FROM virtual_users WHERE email LIKE %s ORDER BY email;", ('%' + search + '%',))
		rows = []
		if len(items) == 0:
			rows.append(("Empty list", 0))
		else:
			for item in items:
				domain = item[0].split("@")[1]
				login = item[0].split("@")[0]
				dir = Config.settings.get('mailboxes', 'dir')
				dir = dir.replace('%domain%', domain)
				dir = dir.replace('%email%', item[0])
				dir = dir.replace('%login%', login)
				stream = os.popen('du -sm ' + dir + ' 2>/dev/null | cut -f1')
				size = stream.read()
				if size == "":
					size = "0"
				if int(size) > item[1] * 0.8:
					s = snok
				else:
					s = sok
				rows.append((s + item[0] + " (" + size.strip() + "MB/" + str(item[1]) + "MB)", 0))
		if search == "":
			filter = ""
		else:
			filter = 'Actual filter: %' + search + '%\n'
		dmenu = ListboxChoiceWindow(screen, 'REPORT QUOTA', filter + 'Mailboxes:', rows, buttons = buttons0, width = 60, scroll = 1, height = 14)
		if dmenu[0] == 'back':
			break
		elif dmenu[0] == 'search':
			searchWindow = EntryWindow(screen, 'Search', '', ['Search:'])
			if searchWindow[0] == 'ok':
				search = searchWindow[1][0]

