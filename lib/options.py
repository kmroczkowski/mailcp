from __future__ import absolute_import, print_function, unicode_literals
from snack import *
import _snack
import sys
from . import utils
from .config import Config
from .db import db

def show(screen):
	bb = ButtonBar(screen, (("Save", "ok"), ('Test DB', 'test'), ("Back", "cancel")))
	text = Label("MySQL configuration:")
	labelDBHost = Label("Host:")
	entryDBHost = Entry(24, Config.settings.get('db', 'host'))
	labelDBPort = Label("Port:")
	entryDBPort = Entry(6, Config.settings.get('db', 'port'))
	labelDB = Label("Database:")
	entryDB = Entry(24, Config.settings.get('db', 'db'))
	labelDBUser = Label("Username:")
	entryDBUser = Entry(24, Config.settings.get('db', 'user'))
	labelDBPassword = Label("Password:")
	entryDBPassword = Entry(24, Config.settings.get('db', 'password'), password = 1)
	text2 = Label("Mailboxes:")
	labelMBDir = Label("Mailboxes dir:")
	entryMBDir = Entry(36, Config.settings.get('mailboxes', 'dir'))
	chbArch = Checkbox("Arch mailboxes when delete", isOn = int(Config.settings.get('mailboxes', 'archive')))
	labelCArch = Label("Command to arch:")
	entryCArch = Entry(36, Config.settings.get('mailboxes', 'archive_command'))
	g = Grid(2, 5)
	g.setField(labelDBHost, 0, 0, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryDBHost, 1, 0)
	g.setField(labelDBPort, 0, 1, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryDBPort, 1, 1, anchorLeft = 1)
	g.setField(labelDB, 0, 2, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryDB, 1, 2)
	g.setField(labelDBUser, 0, 3, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryDBUser, 1, 3)
	g.setField(labelDBPassword, 0, 4, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryDBPassword, 1, 4)
	while True:
		gf = GridForm(screen, "OPTIONS", 1, 10)
		gf.add(text, 0, 0)
		gf.add(g, 0, 1, padding = (0, 0, 0, 1))
		gf.add(text2, 0, 2)
		gf.add(labelMBDir, 0, 3, anchorLeft = 1)
		gf.add(entryMBDir, 0, 4, padding = (0, 0, 0, 1), anchorLeft = 1)
		gf.add(chbArch, 0, 5, anchorLeft = 1)
		gf.add(labelCArch, 0, 6, anchorLeft = 1)
		gf.add(entryCArch, 0, 7, padding = (0, 0, 0, 1), anchorLeft = 1)
		gf.add(bb, 0, 8)
		screen.refresh()
		result = gf.runOnce()
		try:
			port = int(entryDBPort.value())
		except:
			port = 3306
		if port < 100 or port > 65000:
			port = 3306
		if bb.buttonPressed(result) == "ok":
			Config.settings.set('db', 'host', entryDBHost.value())
			Config.settings.set('db', 'port', str(port))
			Config.settings.set('db', 'db', entryDB.value())
			Config.settings.set('db', 'user', entryDBUser.value())
			Config.settings.set('db', 'password', entryDBPassword.value())
			Config.settings.set('mailboxes', 'dir', entryMBDir.value())
			Config.settings.set('mailboxes', 'archive', str(chbArch.value()))
			Config.settings.set('mailboxes', 'archive_command', entryCArch.value())
			Config.save()
			utils.InfoBox(screen, "Information", "Configuration is saved!")
			break
		elif bb.buttonPressed(result) == "test":
			if db.connect(entryDBHost.value(), port, entryDB.value(), entryDBUser.value(), entryDBPassword.value()):
				utils.InfoBox(screen, "Information", "Connect to MySQL sucessfull!")
			else:
				utils.InfoBox(screen, "Error", "Connect to MySQL failed!")
			continue
		else:
			break
