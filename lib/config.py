import sys
import configparser
import os.path

class Config(object):
	config_file = os.path.expanduser("~") + "/.mailcp.ini"
	settings = configparser.ConfigParser(allow_no_value=True)

	@staticmethod
	def init():
		f = open(Config.config_file, "w")
		f.write(";Default configuration\n")
		f.write("[db]\n")
		f.write("host = 127.0.0.1\n")
		f.write("port = 3306\n")
		f.write("db = mailcp\n")
		f.write("user = mailcp\n")
		f.write("password = passwd123456\n")
		f.write("\n")
		f.write("[mailboxes]\n")
		f.write("dir = /var/mail/%domain%/%login%\n")
		f.write("quota = 1024\n")
		f.write("archive = 0\n")
		f.write("archive_command = tar czf /path/to/backup/%email%_%date%_%time%.tgz %mailboxdir%\n")
		f.write("mail_log = /var/log/mail.log\n")
		f.write("\n")
		f.close()

	@staticmethod
	def load():
		Config.settings._interpolation = configparser.ExtendedInterpolation()
		Config.settings.read(Config.config_file)
		if Config.settings.get('mailboxes', 'mail_log', fallback = False) == False:
			Config.settings.set('mailboxes', 'mail_log', '/var/log/mail.log')
			Config.save()

	@staticmethod
	def save():
		f = open(Config.config_file, "w")
		Config.settings.write(f)
		f.close()
