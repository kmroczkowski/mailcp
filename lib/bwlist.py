from __future__ import absolute_import, print_function, unicode_literals
from snack import *
import _snack
import sys
from . import utils
from .db import db

def bwAdd(screen):
	text = TextboxReflowed(40, "Network examples:\n    1.2.3.4 or 1.2.3.0/24\nEmail examples:\n    domain.ltd or .domain.ltd\n    user@domain.ltd or user@")
	labelSource = Label("Source:")
	entrySource = Entry(34, "")
	labelAccess = Label("Access:")
	r1Access = SingleRadioButton("OK ", None, 0)
	r2Access = SingleRadioButton("REJECT", r1Access, 0)
	ga = Grid(2, 1)
	ga.setField(r1Access, 0, 0)
	ga.setField(r2Access, 1, 0)
	labelSourceType = Label("Source type:")
	r1SourceType = SingleRadioButton("IP ", None, 0)
	r2SourceType = SingleRadioButton("EMAIL", r1SourceType, 0)
	gst = Grid(2, 1)
	gst.setField(r1SourceType, 0, 0)
	gst.setField(r2SourceType, 1, 0)
	labelReject = Label("Reject reason:")
	entryReject = Entry(34, "")
	bb = ButtonBar(screen, (("Save", "ok"), ("Back", "cancel")))
	g = Grid(2, 4)
	g.setField(labelSource, 0, 0, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entrySource, 1, 0, anchorLeft = 1)
	g.setField(labelSourceType, 0, 1, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(gst, 1, 1, anchorLeft = 1)
	g.setField(labelAccess, 0, 2, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(ga, 1, 2, anchorLeft = 1)
	g.setField(labelReject, 0, 3, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryReject, 1, 3, anchorLeft = 1)
	while True:
		gf = GridForm(screen, "ADD Black/White list", 1, 3)
		gf.add(text, 0, 0)
		gf.add(g, 0, 1, padding = (0, 1, 0, 1))
		gf.add(bb, 0, 2)
		screen.refresh()
		result = gf.runOnce()
		if bb.buttonPressed(result) == "ok":
			if r1SourceType.selected():
				sourceType = "IP"
			else:
				sourceType = "EMAIL"
			if r1Access.selected():
				access = "OK"
			else:
				access = "REJECT"
			entrySource.set(entrySource.value().lower())
			exists = db.selectone("SELECT COUNT(*) FROM virtual_black_white_list WHERE source = %s;", (entrySource.value(),))
			if len(entrySource.value()) < 4:
				utils.InfoBox(screen, "Error", "Source must minimum 4 chars!")
				continue
			elif len(entrySource.value()) > 32:
				utils.InfoBox(screen, "Error", "Source is too long - 32 chars is max!")
				continue
			elif len(entryReject.value()) > 128:
				utils.InfoBox(screen, "Error", "Reject reason is too long - 128 chars is max!")
				continue
			elif exists[0] > 0:
				utils.InfoBox(screen, "Error", "Source is exists!")
				continue
			else:
				if access == "OK":
					db.insert("INSERT INTO virtual_black_white_list (source, access, type) VALUES (%s, 'OK', %s);", (entrySource.value(), sourceType))
				else:
					if entryReject.value() == "":
						entryReject.set("You are blacklisted!")
					db.insert("INSERT INTO virtual_black_white_list (source, access, reason, type) VALUES (%s, 'REJECT', %s, %s);", (entrySource.value(), entryReject.value(), sourceType))
				utils.InfoBox(screen, "Information", "Lists is save!")
				break
		else:
			break

def bwList(screen):
	search = ""
	filter = ""
	while True:
		items = db.select("SELECT id, source, access FROM virtual_black_white_list WHERE source LIKE %s OR access LIKE %s OR reason LIKE %s ORDER BY source;", ('%' + search + '%', '%' + search + '%', '%' + search + '%'))
		rows = []
		if len(items) == 0:
			rows.append(("Empty list", 0))
			buttons0 = ('Add', 'Search', 'Back')
		else:
			buttons0 = ('Add', 'View', 'Search', 'Delete', 'Back')
			for item in items:
				rows.append((item[1] + ": " + item[2], item[0]))
		if search == "":
			filter = ""
		else:
			filter = 'Actual filter: %' + search + '%\n'
		dmenu = ListboxChoiceWindow(screen, 'BLACK AND WHITE LISTS', filter + 'List domains/IP:', rows, buttons = buttons0, width = 50, scroll = 1, height = 11)
		if dmenu[0] == 'back':
			break
		elif dmenu[0] == 'search':
			searchWindow = EntryWindow(screen, 'Search', '', ['Search:'])
			if searchWindow[0] == 'ok':
				search = searchWindow[1][0]
		elif dmenu[0] == 'delete':
			bw0 = db.selectone("SELECT source, access FROM virtual_black_white_list WHERE id = %s;", (dmenu[1],))
			answer = utils.InfoBoxYesNo(screen, "Warning", "Do you really delete: " + bw0[0] + ': ' + bw0[1])
			if answer == "yes":
				db.delete("DELETE FROM virtual_black_white_list WHERE id = %s", (dmenu[1],))
				utils.InfoBox(screen, "Information", "Domain/IP: " + bw0[0] + ': ' + bw0[1] + " deleted!")
		elif dmenu[0] == 'view':
			bwView(screen, dmenu[1])
		else:
			bwAdd(screen)

def bwView(screen, id):
	bw = db.selectone("SELECT source, access, reason, type FROM virtual_black_white_list WHERE id = %s;", (id,))
	labelSource = Label("Source:")
	labelSourceV = Label(bw[0])
	labelSourceV.setColors(_snack.COLORSET_TITLE)
	labelAccess = Label("Access:")
	labelAccessV = Label(bw[1])
	labelAccessV.setColors(_snack.COLORSET_TITLE)
	labelReason = Label("Reject reason:")
	labelReasonV = Label(bw[2])
	labelReasonV.setColors(_snack.COLORSET_TITLE)
	labelSourceType = Label("Source type:")
	labelSourceTypeV = Label(bw[3])
	labelSourceTypeV.setColors(_snack.COLORSET_TITLE)
	btBack = Button("Back")
	if bw[1] == "OK":
		g = Grid(2, 3)
		list = "White"
	else:
		g = Grid(2, 4)
		g.setField(labelReason, 0, 3, anchorRight = 1, padding = (0, 0, 1, 0))
		g.setField(labelReasonV, 1, 3, anchorLeft = 1)
		list = "Black"
	g.setField(labelSource, 0, 0, anchorRight = 1, padding = (0, 0, 1, 0))
	g.setField(labelSourceV, 1, 0, anchorLeft = 1)
	g.setField(labelSourceType, 0, 1, anchorRight = 1, padding = (0, 0, 1, 0))
	g.setField(labelSourceTypeV, 1, 1, anchorLeft = 1)
	g.setField(labelAccess, 0, 2, anchorRight = 1, padding = (0, 0, 1, 0))
	g.setField(labelAccessV, 1, 2, anchorLeft = 1)
	gf = GridForm(screen, "View " + list  + " list", 1, 3)
	gf.add(g, 0, 1, padding = (0, 0, 0, 1))
	gf.add(btBack, 0, 2)
	screen.refresh()
	result = gf.runOnce()
