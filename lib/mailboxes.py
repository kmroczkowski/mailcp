from __future__ import absolute_import, print_function, unicode_literals
from snack import *
import _snack
import sys
import os
import os.path
import datetime
from . import utils
from .config import Config
from .db import db

def mailboxAdd(screen):
	labelLogin = Label("Login:")
	entryLogin = Entry(23, "")
	labelDomain = Label("Domain:")
	listDomain = Listbox(height = 3, width = 24, scroll = 1, border = 0)
	items = db.select("SELECT id, name FROM virtual_domains ORDER BY name;")
	if len(items) == 0:
		utils.InfoBox(screen, "Information", "No domains found! Please first add domain")
		return
	for item in items:
		listDomain.append(item[1], item[0])
	labelPassword = Label("Password:")
	entryPassword = Entry(23, "", password = 1)
	labelPassword2 = Label("Retype password:")
	entryPassword2 = Entry(23, "", password = 1)
	labelActive = Label("Active:")
	chbActive = Checkbox("", isOn = 1)
	labelQuota = Label("Quota (MB):")
	entryQuota = Entry(7, "1024")
	bb = ButtonBar(screen, (("Save", "ok"), ("Back", "cancel")))
	text = TextboxReflowed(40, "Max length of login to 63 chars. Don't use special chars! Password must have minimum 12 chars! Quota value: 1..99999.")
	g = Grid(2, 6)
	g.setField(labelLogin, 0, 0, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryLogin, 1, 0)
	g.setField(labelDomain, 0, 1, padding = (0, 0, 1, 0), anchorRight = 1, anchorTop = 1)
	g.setField(listDomain, 1, 1)
	g.setField(labelPassword, 0, 2, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryPassword, 1, 2)
	g.setField(labelPassword2, 0, 3, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryPassword2, 1, 3)
	g.setField(labelQuota, 0, 4, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryQuota, 1, 4, anchorLeft = 1)
	g.setField(labelActive, 0, 5, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(chbActive, 1, 5, anchorLeft = 1)
	while True:
		gf = GridForm(screen, "ADD MAILBOX", 1, 3)
		gf.add(text, 0, 0)
		gf.add(g, 0, 1, padding = (0, 1, 0, 1))
		gf.add(bb, 0, 2)
		result = gf.runOnce()
		if bb.buttonPressed(result) == "ok":
			entryLogin.set(entryLogin.value().lower())
			selDomain = db.selectone("SELECT name FROM virtual_domains WHERE id = %s;", (listDomain.current(),))
			exists = db.selectone("SELECT COUNT(*) FROM virtual_users WHERE email = %s;", (entryLogin.value() + '@' + selDomain[0],))
			quota = 0
			try:
				quota = int(entryQuota.value())
			except:
				quota = 0
			if len(entryLogin.value()) > 63:
				utils.InfoBox(screen, "Error", "Login is too long!")
				continue
			elif utils.is_valid_login(entryLogin.value()) == None:
				utils.InfoBox(screen, "Error", "Login is not valid!")
				continue
			elif exists[0] > 1:
				utils.InfoBox(screen, "Error", "Email is aready exists!")
				continue
			elif len(entryPassword.value()) < 12:
				utils.InfoBox(screen, "Error", "Password is too short!")
				continue
			elif entryPassword.value() != entryPassword2.value():
				utils.InfoBox(screen, "Error", "Password and retype password is not equal!")
				continue
			elif quota > 99999 or quota < 1:
				utils.InfoBox(screen, "Error", "Wrong quota value!")
				continue
			else:
				answer = utils.InfoBoxYesNo(screen, "Question", "Please confirm add mailbox: " + entryLogin.value() + '@' + selDomain[0])
				if answer == "yes":
					db.insert("INSERT INTO virtual_users (email, password, quota, active) VALUES (%s, ENCRYPT(%s), %s, %s);", (entryLogin.value() + '@' + selDomain[0], entryPassword.value(), quota, chbActive.value()))
					utils.InfoBox(screen, "Information", "Mailbox is save!")
					break
				else:
					continue
		else:
			break

def mailboxList(screen):
	search = ""
	filter = ""
	while True:
		items = db.select("SELECT id, email FROM virtual_users WHERE email LIKE %s ORDER BY email;", ('%' + search + '%',))
		rows = []
		if len(items) == 0:
			rows.append(('Empty list', 0))
			buttons0 = ('Add', 'Search', 'Back')
		else:
			buttons0 = ('Add', 'Edit', 'Search', 'Delete', 'Back')
			for item in items:
				rows.append((item[1], item[0]))
		if search == "":
			filter = ""
		else:
			filter = 'Actual filter: %' + search + '%\n'
		dmenu = ListboxChoiceWindow(screen, 'MAILBOXES', filter + 'Select mailbox to edit/view details:', rows, buttons = buttons0, width = 50, scroll = 1, height = 11)
		if dmenu[0] == 'back':
			break
		elif dmenu[0] == 'search':
			searchWindow = EntryWindow(screen, 'Search mailbox', '', ['Search:'])
			if searchWindow[0] == 'ok':
				search = searchWindow[1][0]
		elif dmenu[0] == 'edit':
			mailboxView(screen, dmenu[1])
		elif dmenu[0] == 'add':
			mailboxAdd(screen)
		elif dmenu[0] == 'delete':
			mailboxDelete(screen, dmenu[1])
		else:
			if len(items) > 0:
				mailboxView(screen, dmenu[1])
			else:
				mailboxAdd(screen)

def mailboxView(screen, id):
	email = db.selectone("SELECT email, quota, active FROM virtual_users WHERE id = %s;", (id, ))
	domain = email[0].split("@")[1]
	login = email[0].split("@")[0]
	dir = Config.settings.get('mailboxes', 'dir')
	dir = dir.replace('%domain%', domain)
	dir = dir.replace('%email%', email[0])
	dir = dir.replace('%login%', login)
	addcol = 0
	if len(dir) > 23:
		addcol = len(dir) - 23
	labelEmail = Label("Email:")
	labelEmailV = Label(email[0])
	labelEmailV.setColors(_snack.COLORSET_TITLE)
	labelAliases = Label("Aliases:")
	listAliases = Listbox(height = 3, width = 24 + addcol, scroll = 1, border = 0)
	items = db.select("SELECT id, source FROM virtual_aliases WHERE destination = %s ORDER BY source;", (email[0],))
	if len(items) == 0:
		listAliases.append("Empty list", 0)
	else:
		for item in items:
			listAliases.append(item[1], item[0])
	labelPassword = Label("Password:")
	entryPassword = Entry(23 + addcol, "", password = 1)
	labelPassword2 = Label("Retype password:")
	entryPassword2 = Entry(23 + addcol, "", password = 1)
	labelActive = Label("Active:")
	chbActive = Checkbox("", isOn = email[2])
	labelQuota = Label("Quota (MB):")
	entryQuota = Entry(7, str(email[1]))
	labelDir = Label("Mailbox dir:")
	labelDirV = Label(dir)
	labelDirV.setColors(_snack.COLORSET_TITLE)
	labelFiles = Label("Files:")
	stream = os.popen('find ' + dir + ' -type f 2>/dev/null | wc -l')
	files = stream.read()
	stream = os.popen('du -sm ' + dir + ' 2>/dev/null | cut -f1')
	size = stream.read()
	if size =="":
		size = "0"
	if files == "":
		files = "0"
	labelFilesV = Label(files)
	labelFilesV.setColors(_snack.COLORSET_TITLE)
	labelSize = Label("Size mailbox (MB):")
	labelSizeV = Label(size)
	labelSizeV.setColors(_snack.COLORSET_TITLE)
	bb = ButtonBar(screen, (("Save", "ok"), ('Show sent', 'shows'), ('Show received', 'showr'), ("Back", "cancel")))
	text = TextboxReflowed(40, "Password must have minimum 12 chars! Quota value: 1..99999.")
	g = Grid(2, 9)
	g.setField(labelEmail, 0, 0, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(labelEmailV, 1, 0, anchorLeft = 1)
	g.setField(labelAliases, 0, 1, padding = (0, 0, 1, 0), anchorRight = 1, anchorTop = 1)
	g.setField(listAliases, 1, 1, anchorLeft = 1)
	g.setField(labelPassword, 0, 2, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryPassword, 1, 2, anchorLeft = 1)
	g.setField(labelPassword2, 0, 3, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryPassword2, 1, 3, anchorLeft = 1)
	g.setField(labelQuota, 0, 4, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(entryQuota, 1, 4, anchorLeft = 1)
	g.setField(labelDir, 0, 5, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(labelDirV, 1, 5, anchorLeft = 1)
	g.setField(labelSize, 0, 6, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(labelSizeV, 1, 6, anchorLeft = 1)
	g.setField(labelFiles, 0, 7, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(labelFilesV, 1, 7, anchorLeft = 1)
	g.setField(labelActive, 0, 8, padding = (0, 0, 1, 0), anchorRight = 1)
	g.setField(chbActive, 1, 8, anchorLeft = 1)
	while True:
		gf = GridForm(screen, "EDIT MAILBOX", 1, 3)
		gf.add(text, 0, 0)
		gf.add(g, 0, 1, padding = (0, 1, 0, 1))
		gf.add(bb, 0, 2)
		result = gf.runOnce()
		if bb.buttonPressed(result) == "ok":
			quota = 0
			try:
				quota = int(entryQuota.value())
			except:
				quota = 0
			if len(entryPassword.value()) > 0 or len(entryPassword2.value()) > 0:
				if len(entryPassword.value()) < 12:
					utils.InfoBox(screen, "Error", "Password is too short!")
					continue
				elif entryPassword.value() != entryPassword2.value():
					utils.InfoBox(screen, "Error", "Password and retype password is not equal!")
					continue
			if quota > 99999 or quota < 1:
				utils.InfoBox(screen, "Error", "Wrong quota value!")
				continue
			else:
				if len(entryPassword.value()) == 0:
					db.update("UPDATE virtual_users SET quota = %s, active = %s WHERE id = %s;", (quota, chbActive.value(), id))
				else:
					db.update("UPDATE virtual_users SET password = ENCRYPT(%s), quota = %s, active = %s WHERE id = %s;", (entryPassword.value(), quota, chbActive.value(), id))
				utils.InfoBox(screen, "Information", "Mailbox is save!")
				break
		elif bb.buttonPressed(result) == "shows":
			showTraffic(screen, email[0])
		elif bb.buttonPressed(result) == "showr":
			showTraffic(screen, email[0], False)
		else:
			break

def showTraffic(screen, email, sent = True):
	lines = []
	rows = []
	traffic = ''
	with open(Config.settings.get('mailboxes', 'mail_log'), 'r') as fp:
		for line in fp:
			lines.append(line)
	if sent:
		emailfrom = "from=<" + email + ">"
		for line in lines:
			if line.find(emailfrom) != -1 and line.find('postfix/qmgr') != -1:
				linea = line.split()
				to = ""
				for line2 in lines:
					if line2.find(linea[3]) != -1 and line2.find('to=') != -1:
						to = line2.split()[4]
						date = line2.split()[0]
						break
				if to.rstrip() != "":
					rows.append((date[0:19] + ' ' + to[4:-2], 0))
		traffic = 'sent'
	else:
		emailto = "to=<" + email + ">"
		for line in lines:
			if line.find(emailto) != -1 and line.find('postfix/lmtp') != -1:
				linea = line.split()
				from0 = ""
				for line2 in lines:
					if line2.find(linea[3]) != -1 and line2.find('from=') != -1 and line2.find('postfix/qmgr') != -1:
						from0 = line2.split()[4]
						date = line2.split()[0]
						break
				if from0.rstrip() != "":
					rows.append((date[0:19] + ' ' + from0[6:-2], 0))
		traffic = 'received'
	if len(rows) == 0:
		rows.append(("Empty list", 0))
		buttons0 = ('Back')
	else:
		buttons0 = ("Export", 'Back')
	dmenu = ListboxChoiceWindow(screen, email + ' ' + traffic, 'Logs:', rows, buttons = buttons0, width = 60, scroll = 1, height = 14)
	if dmenu[0] != 'back':
		filename = os.path.expanduser("~") + "/" + email + "_" + traffic + ".txt"
		with open(filename, "w") as text_file:
			for row in rows:
				text_file.write(row[0] + "\n")
			utils.InfoBox(screen, "Information", "Export to file: " + filename)

def mailboxDelete(screen, id):
	email = db.selectone("SELECT email FROM virtual_users WHERE id = %s;", (id,))
	aliases = db.selectone("SELECT COUNT(*) FROM virtual_aliases WHERE destination = %s;", (email[0],))
	if aliases[0] > 0:
		utils.InfoBox(screen, "Error", "You can't delete mailbox: aliases exists!")
	else:
		answer = utils.InfoBoxYesNo(screen, "Warning", "Do you really delete mailbox: " + email[0])
		if answer == "yes":
			db.delete("DELETE FROM virtual_users WHERE id = %s", (id,))
			if Config.settings.get('mailboxes', 'archive') == "1":
				utils.InfoBox(screen, "Information", "Mailbox: " + email[0] + " deleted!\nWait for archivize mailbox!")
				date0 = datetime.datetime.now().strftime("%Y%m%d")
				time0 = datetime.datetime.now().strftime("%H%M%S")
				command = Config.settings.get('mailboxes', 'archive_command')
				command = command.replace('%mailboxdir%', Config.settings.get('mailboxes', 'dir'))
				command = command.replace('%date%', date0)
				command = command.replace('%time%', time0)
				command = command.replace('%email%', email[0])
				command = command.replace('%login%', email[0].split('@')[0])
				command = command.replace('%domain%', email[0].split('@')[1])
				os.popen(command)
			else:
				utils.InfoBox(screen, "Information", "Mailbox: " + email[0] + " deleted!")
