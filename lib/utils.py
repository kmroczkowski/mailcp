from __future__ import absolute_import, print_function, unicode_literals
from snack import *
import sys
import os
import glob
import re
from pathlib import Path

#from https://stackoverflow.com/questions/2532053/validate-a-hostname-string
def is_valid_hostname(hostname):
	if len(hostname) > 64:
		return False
	if hostname[-1] == ".":
		hostname = hostname[:-1] # strip exactly one dot from the right, if present
	allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
	return all(allowed.match(x) for x in hostname.split("."))

def is_valid_login(login):
	if len(login) > 63:
		return False
	return re.match("^[a-zA-Z0-9.]+$", login)

def log(msg):
	f = open("log.txt", 'a')
	f.write(msg + "\n")
	f.close()

def InfoBox(screen, title, text, button = "Ok"):
	ButtonChoiceWindow(screen, title, text, buttons = [ 'Ok' ])

def InfoBoxYesNo(screen, title, text):
	return ButtonChoiceWindow(screen, title, text, buttons = [ 'Yes', 'No' ])
