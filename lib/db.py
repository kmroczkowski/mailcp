import sys
import mysql.connector
import traceback
from .config import Config
from .utils import *

class db(object):
	mydb = None
	mycursor = None

	@staticmethod
	def connect(host="", port="", dbname="", user="", password=""):
		if host == "":
			host = Config.settings.get('db', 'host')
			port = int(Config.settings.get('db', 'port'))
			dbname = Config.settings.get('db', 'db')
			user = Config.settings.get('db', 'user')
			password = Config.settings.get('db', 'password')
		try:
			db.mydb = mysql.connector.connect(host=host, port=port, user=user, password=password, database=dbname)
			db.mycursor = db.mydb.cursor()
			db.isConnect = True
		except:
			db.isConnect = False
			#log(traceback.format_exc())
		finally:
			return db.isConnect

	@staticmethod
	def insert(sql, val):
		db.mycursor.execute(sql, val)
		db.mydb.commit()
		return db.mycursor.lastrowid

	@staticmethod
	def update(sql, val):
		db.mycursor.execute(sql, val)
		db.mydb.commit()

	@staticmethod
	def delete(sql, val):
		db.mycursor.execute(sql, val)
		db.mydb.commit()

	@staticmethod
	def select(sql, val = ""):
		if val == "":
			db.mycursor.execute(sql)
		else:
			db.mycursor.execute(sql, val)
		return db.mycursor.fetchall()

	@staticmethod
	def selectone(sql, val = ""):
		if val == "":
			db.mycursor.execute(sql)
		else:
			db.mycursor.execute(sql, val)
		return db.mycursor.fetchone()
