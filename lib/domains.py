from __future__ import absolute_import, print_function, unicode_literals
from snack import *
import _snack
import sys
from . import utils
from .db import db

def domainAdd(screen):
	labelDomain = Label("Domain:")
	entryDomain = Entry(24, "")
	bb = ButtonBar(screen, (("Save", "ok"), ("Back", "cancel")))
	text = TextboxReflowed(25, "Max length of domain to 64 chars. Don't use special chars!")
	g = Grid(2, 1)
	g.setField(labelDomain, 0, 0, padding = (0, 0, 1, 0))
	g.setField(entryDomain, 1, 0)
	while True:
		gf = GridForm(screen, "ADD DOMAIN", 1, 3)
		gf.add(text, 0, 0)
		gf.add(g, 0, 1, padding = (0, 2, 0, 2))
		gf.add(bb, 0, 2)
		screen.refresh()
		result = gf.runOnce()
		if bb.buttonPressed(result) == "ok":
			entryDomain.set(entryDomain.value().lower())
			exists = db.selectone("SELECT COUNT(*) FROM virtual_domains WHERE name = %s;", (entryDomain.value(),))
			if exists[0] > 0:
				utils.InfoBox(screen, "Error", "Domain is already exists!")
				continue
			elif len(entryDomain.value()) > 64:
				utils.InfoBox(screen, "Error", "Domain is too long!")
				continue
			else:
				if utils.is_valid_hostname(entryDomain.value()):
					db.insert("INSERT INTO virtual_domains (name) VALUES (%s);", (entryDomain.value(),))
					utils.InfoBox(screen, "Information", "Domain is save!")
					break
				else:
					utils.InfoBox(screen, "Error", "Domain is not valid!")
					continue
		else:
			break

def domainList(screen):
	search = ""
	filter = ""
	while True:
		items = db.select("SELECT id, name FROM virtual_domains WHERE name LIKE %s ORDER BY name;", ('%' + search + '%',))
		rows = []
		if len(items) == 0:
			rows.append(('Empty list', 0))
			buttons0 = ('Add', 'Search', 'Back')
		else:
			buttons0 = ('Add', 'View', 'Search', 'Delete', 'Back')
			for item in items:
				rows.append((item[1], item[0]))
		if search == "":
			filter = ""
		else:
			filter = 'Actual filter: %' + search + '%\n'
		dmenu = ListboxChoiceWindow(screen, 'VIEW DOMAINS', filter + 'Select domain to view details:', rows, buttons = buttons0, width = 50, scroll = 1, height = 11)
		if dmenu[0] == 'back':
			break
		elif dmenu[0] == 'search':
			searchWindow = EntryWindow(screen, 'Search mailbox', '', ['Search:'])
			if searchWindow[0] == 'ok':
				search = searchWindow[1][0]
		elif dmenu[0] == 'add':
			domainAdd(screen)
		elif dmenu[0] == 'delete':
			domainDelete(screen, dmenu[1])
		else:
			if len(items) == 0:
				domainAdd(screen)
			else:
				domainView(screen, dmenu[1])

def domainView(screen, id):
	labelDomain = Label("Domain:")
	domain = db.selectone("SELECT name FROM virtual_domains WHERE id = %s;", (id,))
	labelDomainV = Label(domain[0])
	labelDomainV.setColors(_snack.COLORSET_TITLE)
	labelCount = Label("Count mailboxes:")
	items = db.selectone("SELECT COUNT(*) FROM virtual_users WHERE email LIKE %s;", ('%@' + domain[0],))
	labelCountV = Label(str(items[0]))
	labelCountV.setColors(_snack.COLORSET_TITLE)
	labelAliases = Label("Count aliases:")
	items = db.selectone("SELECT COUNT(*) FROM virtual_aliases WHERE source LIKE %s;", ('%@' + domain[0],))
	labelAliasesV = Label(str(items[0]))
	labelAliasesV.setColors(_snack.COLORSET_TITLE)
	btBack = Button("Back")
	g = Grid(2, 3)
	g.setField(labelDomain, 0, 0, padding = (0, 0, 1, 0), anchorLeft = 1)
	g.setField(labelDomainV, 1, 0, anchorLeft = 1)
	g.setField(labelCount, 0, 1, padding = (0, 1, 1, 0), anchorLeft = 1)
	g.setField(labelCountV, 1, 1,padding = (0, 1, 0, 0), anchorRight = 1)
	g.setField(labelAliases, 0, 2, padding = (0, 1, 1, 0), anchorLeft = 1)
	g.setField(labelAliasesV, 1, 2,padding = (0, 1, 0, 0), anchorRight = 1)
	gf = GridForm(screen, "VIEW DOMAIN", 1, 2)
	gf.add(g, 0, 0, padding = (0, 1, 0, 2))
	gf.add(btBack, 0, 1)
	result = gf.runOnce()

def domainDelete(screen, id):
	domain = db.selectone("SELECT name FROM virtual_domains WHERE id = %s;", (id,))
	mailboxes = db.selectone("SELECT COUNT(*) FROM virtual_users WHERE email LIKE %s;", ('%@' + domain[0],))
	aliases = db.selectone("SELECT COUNT(*) FROM virtual_aliases WHERE source LIKE %s;", ('%@' + domain[0],))
	if mailboxes[0] > 0:
		utils.InfoBox(screen, "Error", "You can't delete domain: mailboxes exists!")
	elif aliases[0] > 0:
		utils.InfoBox(screen, "Error", "You can't delete domain: aliases exists!")
	else:
		answer = utils.InfoBoxYesNo(screen, "Warning", "Do you really delete domain: " + domain[0])
		if answer == "yes":
			db.delete("DELETE FROM virtual_domains WHERE id = %s", (id,))
			utils.InfoBox(screen, "Information", "Domain: " + domain[0] + " deleted!")
