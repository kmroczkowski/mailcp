from __future__ import absolute_import, print_function, unicode_literals
from snack import *
import _snack
import sys
from . import utils
from .db import db

def aliasAdd(screen):
	labelAlias = Label("Alias:")
	entryAlias = Entry(24, "")
	bb = ButtonBar(screen, (("Save", "ok"), ("Back", "cancel")))
	text = TextboxReflowed(25, "Max length of alias to 63 chars. Don't use special chars!")
	labelDestination = Label("Destination")
	listDestination = Listbox(height = 3, width = 40, scroll = 1, border = 0)
	items = db.select("SELECT id, email FROM virtual_users ORDER BY email;")
	if len(items) == 0:
		utils.InfoBox(screen, "Information", "No domains found! Please first add domain")
		return
	addcol = 0
	for item in items:
		listDestination.append(item[1], item[0])
	labelDomain = Label("Domain:")
	listDomain = Listbox(height = 3, width = 40, scroll = 1, border = 0)
	items = db.select("SELECT id, name FROM virtual_domains ORDER BY name;")
	if len(items) == 0:
		utils.InfoBox(screen, "Information", "No domains found! Please first add domain")
		return
	for item in items:
		listDomain.append(item[1], item[0])
	g = Grid(2, 3)
	g.setField(labelAlias, 0, 0, padding = (0, 0, 1, 0))
	g.setField(entryAlias, 1, 0, anchorLeft = 1)
	g.setField(labelDomain, 0, 1, padding = (0, 0, 1, 0))
	g.setField(listDomain, 1, 1)
	g.setField(labelDestination, 0, 2, padding = (0, 0, 1, 0))
	g.setField(listDestination, 1, 2)
	while True:
		gf = GridForm(screen, "ADD ALIAS", 1, 3)
		gf.add(text, 0, 0)
		gf.add(g, 0, 1, padding = (0, 2, 0, 2))
		gf.add(bb, 0, 2)
		screen.refresh()
		result = gf.runOnce()
		if bb.buttonPressed(result) == "ok":
			entryAlias.set(entryAlias.value().lower())
			selDomain = db.selectone("SELECT name FROM virtual_domains WHERE id = %s;", (listDomain.current(),))
			selEmail = db.selectone("SELECT email FROM virtual_users WHERE id = %s;", (listDestination.current(),))
			exists = db.selectone("SELECT COUNT(*) FROM virtual_users WHERE email = %s;", (entryAlias.value() + '@' + selDomain[0],))
			exists2 = db.selectone("SELECT COUNT(*) FROM virtual_aliases WHERE source = %s;", (entryAlias.value() + '@' + selDomain[0],))
			if exists[0] > 0:
				utils.InfoBox(screen, "Error", "Mailbox with this login and domain is exists!")
				continue
			elif exists2[0] > 0:
				utils.InfoBox(screen, "Error", "Alias with this login and domain is already exists!")
				continue
			elif len(entryAlias.value()) > 63:
				utils.InfoBox(screen, "Error", "Alias is too long!")
				continue
			elif utils.is_valid_login(entryAlias.value()) == None:
				utils.InfoBox(screen, "Error", "Alias is not valid!")
				continue
			else:
				db.insert("INSERT INTO virtual_aliases (source, destination) VALUES (%s, %s);", (entryAlias.value() + '@' + selDomain[0], selEmail[0]))
				utils.InfoBox(screen, "Information", "Alias is save!")
				break
		else:
			break

def aliasesList(screen):
	search = ""
	filter = ""
	while True:
		items = db.select("SELECT id, source, destination FROM virtual_aliases WHERE source LIKE %s OR destination LIKE %s ORDER BY source;", ('%' + search + '%', '%' + search + '%'))
		rows = []
		if len(items) == 0:
			rows.append(("Empty list", 0))
			buttons0 = ('Add', 'Search', 'Back')
		else:
			buttons0 = ('Add', 'Search', 'Delete', 'Back')
			for item in items:
				rows.append((item[1] + "->" + item[2], item[0]))
		if search == "":
			filter = ""
		else:
			filter = 'Actual filter: %' + search + '%\n'
		dmenu = ListboxChoiceWindow(screen, 'LIST ALIASES', filter + 'List all aliases:', rows, buttons = buttons0, width = 50, scroll = 1, height = 11)
		if dmenu[0] == 'back':
			break
		elif dmenu[0] == 'search':
			searchWindow = EntryWindow(screen, 'Search mailbox', '', ['Search:'])
			if searchWindow[0] == 'ok':
				search = searchWindow[1][0]
		elif dmenu[0] == 'delete':
			alias0 = db.selectone("SELECT source, destination FROM virtual_aliases WHERE id = %s;", (dmenu[1],))
			answer = utils.InfoBoxYesNo(screen, "Warning", "Do you really delete alias: " + alias0[0] + '->' + alias0[1])
			if answer == "yes":
				db.delete("DELETE FROM virtual_aliases WHERE id = %s", (dmenu[1],))
				utils.InfoBox(screen, "Information", "Alias: " + alias0[0] + '->' + alias0[1] + " deleted!")
		else:
			aliasAdd(screen)
