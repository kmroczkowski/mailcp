Install
-------

Scheme for DB MySQL:
```
CREATE TABLE virtual_aliases (
  id int(11) NOT NULL AUTO_INCREMENT,
  source varchar(128) NOT NULL,
  destination varchar(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE virtual_domains (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(64) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE virtual_users (
  id int(11) NOT NULL AUTO_INCREMENT,
  email varchar(128) NOT NULL,
  password varchar(128) NOT NULL,
  quota int(11) NOT NULL DEFAULT '1024' COMMENT 'Quota in megabytes',
  active tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (id)
);

CREATE TABLE virtual_black_white_list (
  id int(11) NOT NULL AUTO_INCREMENT,
  source varchar(32) NOT NULL COMMENT 'Domain or IP address',
  access enum('OK','REJECT') NOT NULL COMMENT 'OK or REJECT',
  reason varchar(128) NOT NULL DEFAULT '',
  `type` enum('IP','EMAIL') NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY source (source)
);
```

Install debian packages:
```
apt install git python-newt python-pip python-pathlib
pip install mysql-connector-python
git clone https://gitlab.com/kmroczkowski/mailcp.git
```

Run:
```
cd mailcp
./mailcp
```

Screenshots:
-------

![menu](/screenshots/menu.png?raw=true "menu")

![options](/screenshots/options.png?raw=true "options")

![report](/screenshots/report.png?raw=true "report")

![domains](/screenshots/domains.png?raw=true "domains")

![mailboxes](/screenshots/mailboxes.png?raw=true "mailboxes")

![addmailbox](/screenshots/addmailbox.png?raw=true "addmailbox")

![meeditmailboxnu](/screenshots/editmailbox.png?raw=true "editmailbox")

![aliases](/screenshots/aliases.png?raw=true "aliases")

![addmailbox](/screenshots/addmailbox.png?raw=true "addmailbox")

![viewdomain](/screenshots/viewdomain.png?raw=true "viewdomain")
